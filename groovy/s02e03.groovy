/* Añade aquí la implementación solicitada en el ejercicio */

def moneda(String locale)
{
    if(locale == "en_EN")
    {
        return "£"
    }
    
    if(locale == "en_US")
    {
        return "\$"
    }
    
    if(locale == "es_ES")
    {
        return "€"
    }
}

BigDecimal.metaClass.moneda = { locale ->
    if(locale == "en_EN" || locale == "en_US")
    {
        return moneda(locale) + "${delegate}"
    }
    else
    {
        return "${delegate}" + moneda(locale)
    }
}

Integer.metaClass.moneda = { locale ->
    if(locale == "en_EN" || locale == "en_US")
    {
        return moneda(locale) + "${delegate}"
    }
    else
    {
        return "${delegate}" + moneda(locale)
    }
}

Float.metaClass.moneda = { locale ->
    if(locale == "en_EN" || locale == "en_US")
    {
        return moneda(locale) + "${delegate}"
    }
    else
    {
        return "${delegate}" + moneda(locale)
    }
}

Double.metaClass.moneda = { locale ->
    if(locale == "en_EN" || locale == "en_US")
    {
        return moneda(locale) + "${delegate}"
    }
    else
    {
        return "${delegate}" + moneda(locale)
    }
}

assert 10.2.moneda("en_EN") ==  "£10.2"
assert 10.2.moneda("en_US") ==  "\$10.2"
assert 10.2.moneda("es_ES") ==  "10.2€"

assert 10.moneda("en_EN") ==  "£10"
assert 10.moneda("en_US") ==  "\$10"
assert 10.moneda("es_ES") ==  "10€"

assert new Float(10.2).moneda("en_EN") ==  "£10.2"
assert new Float(10.2).moneda("en_US") ==  "\$10.2"
assert new Float(10.2).moneda("es_ES") ==  "10.2€"

assert new Double(10.2).moneda("en_EN") ==  "£10.2"
assert new Double(10.2).moneda("en_US") ==  "\$10.2"
assert new Double(10.2).moneda("es_ES") ==  "10.2€"