/* CALCULADORA*/

class Calculadora{

    def suma(x, y){
        return x + y
    }
    
    def resta(x, y){
        return x - y
    }
    
    def multiplicacion(x, y){
        return x * y
    }
    
    def division(x, y){
        return x / y
    }
}

System.in.withReader {
    print 'Introduzca primer operando: '
    op1 = it.readLine()
    println(op1)
    
    print 'Introduzca segundo operando: '
    op2 = it.readLine()
    println(op2)
    
    print 'Introduzca operador: '
    op = it.readLine()
    println(op)
}

def calculadora = new Calculadora()
def resultado

if(op == "+")
{
    resultado = calculadora.suma(op1.toInteger(), op2.toInteger()) 
}
else if (op == "-")
{
    resultado = calculadora.resta(op1.toInteger(), op2.toInteger())
}
else if (op == "*")
{
    resultado = calculadora.multiplicacion(op1.toInteger(), op2.toInteger())
}
else
{
    resultado = calculadora.division(op1.toInteger(), op2.toInteger())
}

println op1 + " " + op + " " + op2 + " = " + (resultado)