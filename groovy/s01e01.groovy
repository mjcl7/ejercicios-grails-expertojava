class Todo {
    String titulo
    String descripcion
}

todos = [new Todo(titulo:"Lavadora", descripcion:"Poner lavadora"), 
         new Todo(titulo:"Impresora",descripcion:"Comprar cartuchos impresora"),
         new Todo(titulo:"Películas",descripcion:"Devolver películas videoclub")]


for (todo in todos) {
    println("${todo.getTitulo()} " + "${todo.getDescripcion()}");
}