/* Añade aquí la implementación del factorial en un closure */
def factorial
factorial = { n ->
    return (n==0) ? 1 : n * factorial(n-1)
}

assert factorial(1)==1
assert factorial(2)==1*2
assert factorial(3)==1*2*3
assert factorial(4)==1*2*3*4
assert factorial(5)==1*2*3*4*5
assert factorial(6)==1*2*3*4*5*6
assert factorial(7)==1*2*3*4*5*6*7
assert factorial(8)==1*2*3*4*5*6*7*8
assert factorial(9)==1*2*3*4*5*6*7*8*9
assert factorial(10)==1*2*3*4*5*6*7*8*9*10

/* Añade a partir de aquí el resto de tareas solicitadas en el ejercicios */

def listaEnteros = [1,2,3,4,5,6,7,8,9,10]

for(elemento in listaEnteros)
{
    println "factorial(" + elemento + ") = " + factorial(elemento)
}

def ayer
ayer = { fecha ->
    return fecha - 1
}

def mañana
mañana = { fecha ->
    return fecha + 1
}

assert ayer (new Date().parse("d/M/yyyy H:m:s","28/6/2008 00:30:20")) == new Date().parse("d/M/yyyy H:m:s","27/6/2008 00:30:20")
assert mañana (new Date().parse("d/M/yyyy H:m:s","28/6/2008 00:30:20")) == new Date().parse("d/M/yyyy H:m:s","29/6/2008 00:30:20")

def listaFechas = [new Date().parse("d/M/yyyy H:m:s","28/10/2016 00:30:20"), 
                   new Date().parse("d/M/yyyy H:m:s","22/5/2016 00:30:20"), 
                   new Date().parse("d/M/yyyy H:m:s","18/3/2016 00:30:20"),
                   new Date().parse("d/M/yyyy H:m:s","31/8/2016 00:30:20")]

for(fecha in listaFechas)
{
    println fecha
    println "AYER " + ayer(fecha) + "    MAÑANA " + mañana(fecha) + "\n"
}