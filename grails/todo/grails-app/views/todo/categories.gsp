
<%@ page import="es.ua.expertojava.todo.Todo" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<a href="#list-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
        <li><g:link action="categories">Categorías</g:link></li>
    </ul>
</div>
<div id="list-todo" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="title" title="${message(code: 'todo.title.label', default: 'Title')}" />

            %{--<g:sortableColumn property="description" title="${message(code: 'todo.description.label', default: 'Description')}" />--}%

            <g:sortableColumn property="date" title="${message(code: 'todo.date.label', default: 'Date')}" />

            %{--<g:sortableColumn property="reminderDate" title="${message(code: 'todo.reminderDate.label', default: 'Reminder Date')}" />--}%

            <th><g:message code="todo.category.label" default="Category" /></th>

            <g:sortableColumn property="done" title="${message(code: 'todo.done.label', default: 'Done')}" />

        </tr>
        </thead>
        <tbody>
        <g:each in="${todoInstanceList}" status="i" var="todoInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">


                <td><g:link action="show" id="${todoInstance.id}">${fieldValue(bean: todoInstance, field: "title")}</g:link></td>

                %{--<td>${fieldValue(bean: todoInstance, field: "description")}</td>--}%

                <td><g:formatDate date="${todoInstance.date}" /></td>

                %{--<td><g:formatDate date="${todoInstance.reminderDate}" /></td>--}%

                <td>${fieldValue(bean: todoInstance, field: "category")}</td>

                <td><todo:printIconFromBoolean value="${todoInstance.done}"/></td>

            </tr>
        </g:each>
        </tbody>
    </table>
    <g:form url="[resource:todoInstance, action:'listByCategory']" method="GET">
        <fieldset class="buttons">
            <g:link action="listByCategory" resource="${todoInstance}"><g:message code="default.button.edit.label" default="List" /></g:link>
        </fieldset>
    </g:form>
    <div class="pagination">
        <g:paginate total="${todoInstanceCount ?: 0}" />
    </div>
</div>
</body>
</html>
