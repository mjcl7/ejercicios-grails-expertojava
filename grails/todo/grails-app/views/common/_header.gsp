<div id="header">
    <div id="menu">
            <sec:ifLoggedIn>
                Welcome <label style="font-weight: bold"><sec:username/></label>
                <g:form controller="logout">
                    <g:submitButton name="Logout" action="index" method="post"></g:submitButton>
                </g:form>
            </sec:ifLoggedIn>
            <sec:ifNotLoggedIn>
                <g:link controller="login" action="auth">Login</g:link>
            </sec:ifNotLoggedIn>
        </nobr>
    </div>
</div>

