<%@ page import="es.ua.expertojava.todo.Tag" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'tag.label', default: 'Tag')}" />
    <title><g:message code="default.button.delete.label" args="[entityName]" /></title>
</head>
<body>
    <h1><g:message code="default.button.delete.label" args="[entityName]" /></h1>

    <table>
        <tr>
            <td>
                <asset:image src="warning.png"></asset:image>
            </td>
            <td style="padding-top: 100px; padding-left: 0px">
                <label style="font-weight: bold; font-size: 40px">¿ESTÁ SEGURO?</label>
            </td>
        </tr>
    </table>

    <g:form url="[resource:tagInstance, action:'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" />
        </fieldset>
    </g:form>

</body>
</html>
