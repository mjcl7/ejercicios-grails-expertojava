import es.ua.expertojava.todo.*

class BootStrap {

    def init = { servletContext ->

        try {
            def rolAdmin = new Role(authority: "ROLE_ADMIN").save()
            def rolBasic = new Role(authority: "ROLE_BASIC").save()

            def usuarioAdmin = new User(username: "admin", password: "admin", name: "admin", surnames: "ad", email: "admin@email.com", confirmPassword: "admin").save()
            def usuario1 = new User(username: "usuario1", password: "usuario1", name: "usuario1", surnames: "usu1", email: "usuario1@email.com", confirmPassword: "usuario1").save()
            def usuario2 = new User(username: "usuario2", password: "usuario2", name: "usuario2", surnames: "usu2", email: "usuario2@email.com", confirmPassword: "usuario2").save()

            PersonRole.create usuarioAdmin, rolAdmin
            PersonRole.create usuario1, rolBasic
            PersonRole.create usuario2, rolBasic

            def categoryHome = new Category(name: "Hogar").save()
            def categoryJob = new Category(name: "Trabajo").save()

            def tagEasy = new Tag(name: "Fácil").save()
            def tagDifficult = new Tag(name: "Difícil").save()
            def tagArt = new Tag(name: "Arte").save()
            def tagRoutine = new Tag(name: "Rutina").save()
            def tagKitchen = new Tag(name: "Cocina").save()

            def todoPaintKitchen = new Todo(title: "Pintar cocina", date: new Date() + 1, done: false)
            def todoCollectPost = new Todo(title: "Recoger correo postal", date: new Date() + 2, done: true)
            def todoBakeCake = new Todo(title: "Cocinar pastel", date: new Date() + 4, done: true)
            def todoWriteUnitTests = new Todo(title: "Escribir tests unitarios", date: new Date(), done: false)

            todoPaintKitchen.addToTags(tagDifficult)
            todoPaintKitchen.addToTags(tagArt)
            todoPaintKitchen.addToTags(tagKitchen)
            todoPaintKitchen.category = categoryHome
            todoPaintKitchen.user = usuario1
            todoPaintKitchen.save()

            todoCollectPost.addToTags(tagRoutine)
            todoCollectPost.category = categoryJob
            todoCollectPost.user = usuario1
            todoCollectPost.save()

            todoBakeCake.addToTags(tagEasy)
            todoBakeCake.addToTags(tagKitchen)
            todoBakeCake.category = categoryHome
            todoBakeCake.user = usuario2
            todoBakeCake.save()

            todoWriteUnitTests.addToTags(tagEasy)
            todoWriteUnitTests.category = categoryJob
            todoWriteUnitTests.user = usuario2
            todoWriteUnitTests.save()
        }
        catch (Exception ex)
        {
            println(ex)
        }
    }
    def destroy = { }
}

