package es.ua.expertojava.todo

class Tag {

    String name
    String color = "#FFFFFF"

    static hasMany = [todos:Todo]

    static constraints = {
        name(blank:false, nullable:false, unique:true)
        color (nullable: true, shared: "rgbcolor")
    }

    String toString(){
        name
    }
}
