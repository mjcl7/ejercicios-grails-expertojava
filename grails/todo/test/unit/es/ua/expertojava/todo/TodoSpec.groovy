package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Todo)
class TodoSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    def "Test que prueba que la fecha de recordatorio nunca puede ser posterior a la fecha de la propia tarea"(){
        given:
        def usuario = new User(username: "usuario1", password: "usuario1", name: "usuario1", surnames: "usu1", email: "usuario1@email.com", confirmPassword: "usuario1")

        def t1 = new Todo(title: "Todo test", date: new Date(), reminderDate: new Date() + 2, user: usuario)
        when:
        t1.validate()
        then:
        t1?.errors["reminderDate"]
    }
}
