package es.ua.expertojava.todo

import asset.pipeline.grails.AssetsTagLib
import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.web.GroovyPageUnitTestMixin} for usage instructions
 */
@TestFor(TodoTagLib)
class TodoTagLibSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "La etiqueta includeJs devuelve una referencia a la librería javascript pasada por parámetro"() {
        expect:
        applyTemplate('<todo:includeJs script="" />') == "<script src='scripts/.js'></script>"
        applyTemplate('<todo:includeJs script="myfile" />') == "<script src='scripts/myfile.js'></script>"
    }


    @Unroll
    void "El método printIconFromBoolean devuelve una ruta a una imagen"() {
        given:
        def assetsTagLib = Mock(AssetsTagLib)
        tagLib.metaClass.asset = assetsTagLib
        when:
        def output = applyTemplate('<todo:printIconFromBoolean value="${value}" />', [value:value])
        then:
        output == expectedOutput
        and:
        1 * assetsTagLib.image(_) >> { value ? "done.png" : "notdone.png" }
        where:
        value   |   expectedOutput
        true    |   "done.png"
        false   |   "notdone.png"
    }



}
