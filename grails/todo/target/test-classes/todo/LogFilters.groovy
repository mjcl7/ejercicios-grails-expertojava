package todo

class LogFilters {

    def filters = {
        all(controller:'todo|category|tag', action:'create|edit|index|show') {
            before = {

            }
            after = { Map model ->
                println "Controlador ${controllerName} - Accion ${actionName} - Modelo ${model}"
            }
            afterView = { Exception e ->

            }
        }
    }
}
